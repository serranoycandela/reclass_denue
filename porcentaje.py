carpeta = "/home/fidel/Documents/ANM/nacional/usv_7"

os.chdir(carpeta)

layer = iface.activeLayer()

def porcentaje(columna, overlay):
    print(columna)
    if columna not in layer.fields().names():
        layer.dataProvider().addAttributes([QgsField(columna, QVariant.Double, 'double', 10, 3)])
        layer.updateFields()

    contador = 0
    layer.startEditing()
    for f in layer.getFeatures():
        contador += 1
        geo_input = f.geometry() 
        area_interseccion = 0 
        for poligono in overlay.getFeatures(): 
            geo_poli = poligono.geometry() 
            if geo_poli.intersects(geo_input): 
                area_interseccion += geo_poli.intersection(geo_input).area() 
        
        
        porciento_interseccion = round(100*(area_interseccion/geo_input.area()),1)
        f[columna] = porciento_interseccion
        layer.updateFeature(f)
        print(contador)
    layer.commitChanges()


porcentaje("acuicola", QgsVectorLayer("acuicola.shp","acuicola","ogr"))
porcentaje("agricola", QgsVectorLayer("agricultura.shp","agricultura","ogr"))
porcentaje("natural", QgsVectorLayer("natural.shp","natural","ogr"))
porcentaje("pastizal", QgsVectorLayer("pastizales.shp","pastizales","ogr"))
porcentaje("urbano", QgsVectorLayer("urbano.shp","urbano","ogr"))
