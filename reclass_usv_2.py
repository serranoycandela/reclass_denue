# este script reclasifica la columna DESCRIPCIO en 3 categorias de agricultura, 
# urbano, pastizales inducidos o cultivados, y natural.
# para correrlo debe estar seleccionada la capa de de uso de suelo en el panel de capas

la_capa = iface.activeLayer() # asignar la capa activa a la variable "la_capa"

#agregar la columna "tipo"
if 'tipo_2' not in la_capa.fields().names():
    la_capa.dataProvider().addAttributes([QgsField('tipo_2', QVariant.String)])
    la_capa.updateFields()
    
la_capa.startEditing()
contador = 0
for f in la_capa.getFeatures(): # recorrer los elementos de la capa
    contador += 1
    cat = f['DESCRIPCIO']
    if "HUME" in cat or "TEMP" in cat or "RIE" in cat:
        su_tipo = "Agricultura"
    elif "ACU" in cat:
        su_tipo = "Acuicultura"
    elif "ASEN" in cat:
        su_tipo = "Urbano"
    elif "PASTIZAL CUL" in cat or "PASTIZAL IN" in cat:
        su_tipo = "Pastizales inducidos o cultivados"
    else:
        su_tipo = "Natural y otros"
    f['tipo_2'] = su_tipo # asignar la categoria agregada a la columna "tipo"
    if contador % 10000 == 0:
        print(contador, cat, " -> ", su_tipo)
    la_capa.updateFeature(f)
la_capa.commitChanges()