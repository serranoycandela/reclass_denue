# este script reclasifica la columna nombre_act en 4 categorias agregadas
# "Petroleo y gas", "Carbón mineral", "Minerales metálicos", "Minerales no metálicos"
# para correrlo debe estar seleccionada la capa de mineria en el panel de capas

la_capa = iface.activeLayer() # asignar la capa activa a la variable "la_capa"

#agregar la columna "tipo"
if 'tipo' not in la_capa.fields().names():
    la_capa.dataProvider().addAttributes([QgsField('tipo', QVariant.String)])
    la_capa.updateFields()
    
la_capa.startEditing()
for f in la_capa.getFeatures(): # recorrer los elementos de la capa
    cat = f['nombre_act']
    if "Extrac" in cat or "petro" in cat:
        su_tipo = "Petroleo y gas"
    elif "carb" in cat:
        su_tipo = "Carbón mineral"
    elif "hierro" in cat or "plata" in cat or "cobre" in cat or "oro" in cat or "manganeso" in cat or "plomo" in cat or "mercurio" in cat or "metales met" in cat:
        su_tipo = "Minerales metálicos"
    else:
        su_tipo = "Minerales no metálicos"
    f['tipo'] = su_tipo # asignar la categoria agregada a la columna "tipo"
    print(cat, " -> ", su_tipo)
    la_capa.updateFeature(f)
la_capa.commitChanges()

    