# este script reclasifica la columna nombre_act en 4 categorias agregadas
# "Petroleo y gas", "Carbón mineral", "Minerales metálicos", "Minerales no metálicos"
# para correrlo debe estar seleccionada la capa de mineria en el panel de capas

la_capa = iface.activeLayer() # asignar la capa activa a la variable "la_capa"

#agregar la columna "tipo"
if 'grupos' not in la_capa.fields().names():
    la_capa.dataProvider().addAttributes([QgsField('grupos', QVariant.String)])
    la_capa.updateFields()
else:
    print("ya estaba")
    
    
la_capa.startEditing()
contador = 0
lista4 = []
lista5 = []
lista6 = []
lista7 = []
lista8 = []
for f in la_capa.getFeatures(): # recorrer los elementos de la capa
    contador += 1
    nombre_act = f['nombre_act']
    grupo = f['grupos']
    if not grupo:
        if "espe" in nombre_act or "azu" in nombre_act or " no met" in nombre_act or "refractarios" in nombre_act or "loz" in nombre_act or "yes" in nombre_act or "canter" in nombre_act or nombre_act == "Fabricación de cal" or "ladr" in nombre_act or "equipo para la co" in nombre_act or "cem" in nombre_act or "concreto" in nombre_act or "asf" in nombre_act or "vidr" in nombre_act:
            if nombre_act not in lista4:
                lista4.append(nombre_act)
            su_grupo = "Grupo 4"
        elif "pot" in nombre_act or "comestibles" in nombre_act or "Pan" in nombre_act or "agua" in nombre_act or "envasado" in nombre_act or "Elaboración" in nombre_act or "cere" in nombre_act or "caf" in nombre_act or "bebidas" in nombre_act or "Bene" in nombre_act or "tabaco" in nombre_act or "alimento" in nombre_act or "azú" in nombre_act or "frut" in nombre_act or "vege" in nombre_act or "cerv" in nombre_act or "choc" in nombre_act or "bota" in nombre_act:
            if nombre_act not in lista5:
                lista5.append(nombre_act)
            su_grupo = "Grupo 5"
        elif "cost" in nombre_act or "ceri" in nombre_act or "Aserr" in nombre_act or "trenz" in nombre_act or "ataú" in nombre_act or "cord" in nombre_act or "corti" in nombre_act or "tabla" in nombre_act or "calc" in nombre_act or "papel" in nombre_act or "cart" in nombre_act or "tap" in nombre_act or "hil" in nombre_act or "mue" in nombre_act or "pulpa" in nombre_act or "tela" in nombre_act or "hule" in nombre_act or "aserr" in nombre_act or "text" in nombre_act or "made" in nombre_act or "blancos" in nombre_act or "papel " in nombre_act or "trajes" in nombre_act or ("vestir" in nombre_act and "cuero" not in nombre_act) or "ropa" in nombre_act or "uniform" in nombre_act:
            if nombre_act not in lista6:
                lista6.append(nombre_act)
            su_grupo = "Grupo 6"
        elif "cám" in nombre_act or "exp" in nombre_act or "gase" in nombre_act or "adh" in nombre_act or "colc" in nombre_act or "lub" in nombre_act or "libro" in nombre_act or "espu" in nombre_act or "cosm" in nombre_act or "dib" in nombre_act or "dese" in nombre_act or "vel" in nombre_act or "escob" in nombre_act or "jug" in nombre_act or "resin" in nombre_act or "jab" in nombre_act or "abra" in nombre_act or "pig" in nombre_act or "pinturas" in nombre_act or "farma" in nombre_act or "camis" in nombre_act or "fert" in nombre_act or "pest" in nombre_act or "petr" in nombre_act or "carb" in nombre_act or ("impre" in nombre_act and "monedas" not in nombre_act) or "quí" in nombre_act or "plást" in nombre_act or "fotografía" in nombre_act:
            if nombre_act not in lista7:
                lista7.append(nombre_act)
            su_grupo = "Grupo 7"
        else:
            if nombre_act not in lista8:
                lista8.append(nombre_act)
            su_grupo = "Grupo 8"
        f['grupos'] = su_grupo
        la_capa.updateFeature(f)
        if contador % 10000 == 0:
            print(contador, nombre_act, " -> ", su_grupo)
            la_capa.commitChanges()
            la_capa.startEditing()
        
    
print("Grupo 4", lista4)
print("Grupo 5", lista5)
print("Grupo 6", lista6)
print("Grupo 7", lista7)
print("Grupo 8", lista8)
la_capa.commitChanges()
print("Ya")