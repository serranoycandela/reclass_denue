# reclass_denue

Este softwere automatiza el proceso de reclasificar de decenas o cientos de categorías unas cuantas que sean útiles para el análisis. En el repositorio hay códigos específicos para reclasificar los datos nacionales proporcionados por el INEGI-DENUE para industrias de extracción y de transformación, y para datos de ganado estabulado y la serie 7 de uso de suelo y vegetación, ambas también de INEGI.

<img src="mapa.png" width="400px">



