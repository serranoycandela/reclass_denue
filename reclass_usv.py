# este script reclasifica la columna DESCRIPCIO en 3 categorias de agricultura, 
# urbano, pastizales inducidos o cultivados, y natural.
# para correrlo debe estar seleccionada la capa de de uso de suelo en el panel de capas

la_capa = iface.activeLayer() # asignar la capa activa a la variable "la_capa"

#agregar la columna "tipo"
if 'tipo' not in la_capa.fields().names():
    la_capa.dataProvider().addAttributes([QgsField('tipo', QVariant.String)])
    la_capa.updateFields()
    
la_capa.startEditing()
contador = 0
for f in la_capa.getFeatures(): # recorrer los elementos de la capa
    contador += 1
    cat = f['DESCRIPCIO']
    if "HUME" in cat:
        su_tipo = "Agricultura de humedad"
    elif "TEMP" in cat:
        su_tipo = "Agricultura de temporal"
    elif "RIE" in cat:
        su_tipo = "Agricultura de riego"
    elif "ACU" in cat:
        su_tipo = "Acuicultura"
    elif "ASEN" in cat:
        su_tipo = "Urbano"
    elif "PASTIZAL CUL" in cat or "PASTIZAL IN" in cat:
        su_tipo = "Pastizales inducidos o cultivados"
    else:
        su_tipo = "Natural y otros"
    f['tipo'] = su_tipo # asignar la categoria agregada a la columna "tipo"
    if contador % 1000 == 0:
        print(contador, cat, " -> ", su_tipo)
    la_capa.updateFeature(f)
la_capa.commitChanges()